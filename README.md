![build_status](https://gitlab.com/apiazza134/org-lab3/badges/master/pipeline.svg)

# Org file di studio per Laboratorio 3
È disponibile l'export dell'`org-file` in
- [pdf](https://gitlab.com/apiazza134/org-lab3/-/jobs/artifacts/master/raw/lab3.pdf?job=org_export)
- [html](https://gitlab.com/apiazza134/org-lab3/-/jobs/artifacts/master/raw/lab3.html?job=org_export)
